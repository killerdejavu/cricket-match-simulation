import random


def weighted_choice(choices, weights):
    return random.choices(population=list(choices), weights=list(weights), k=1)[0]
