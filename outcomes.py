from enum import Enum


class Outcomes(Enum):
    OUT = 'out'
    DOT = 'dot'
    SINGLE = 'single'
    DOUBLE = 'double'
    THREE = 'three'
    FOUR = 'four'
    FIVE = 'five'
    SIX = 'six'


def outcome_to_commentry_text(outcome):
    text = ""
    if outcome == Outcomes.OUT:
        text = "is OUT!"
    elif outcome == Outcomes.DOT:
        text = "just places the ball nearby. Its a dot ball"
    elif outcome == Outcomes.SINGLE:
        text = "scores 1 run"
    elif outcome == Outcomes.DOUBLE:
        text = "scores 2 run"
    elif outcome == Outcomes.THREE:
        text = "scores 3 run"
    elif outcome == Outcomes.FOUR:
        text = "scores 4 run"
    elif outcome == Outcomes.FIVE:
        text = "scores 5 run"
    elif outcome == Outcomes.SIX:
        text = "scores a SIX!"
    return text
