from collections import OrderedDict
from queue import Queue

import helper
import outcomes
from exception import ValidationError
from outcomes import Outcomes


class Match(object):
    def __init__(self, bating_team='Bengaluru', bowling_team='Chennai',
                 total_runs_needed_to_win=40, total_overs_left=4,
                 total_wickets_left=3):
        # Maintain the team names
        self.bating_team = bating_team
        self.bowling_team = bowling_team

        # Maintain the global values
        self.total_runs_needed_to_win = total_runs_needed_to_win
        self.total_wickets_left = total_wickets_left
        self.total_overs_left = total_overs_left

        # To maintain the match table for each player (runs_scored, balls_faced,
        # has_played_yet)
        self._match_table = OrderedDict()

        # This is to maintain the ball by ball log,
        # which helps in creating commentary
        self._ball_log = list()

        # Maintain the players queue
        self._player_queue = Queue()

        # Maintain batsman on and off strike position
        self._batsman_on_strike = None
        self._batsman_off_strike = None

        # Maintain all the changes in runs,
        # overs, wickets, balls which will be
        # affected after every ball
        self._runs_left_to_win = total_runs_needed_to_win
        self._overs_left = total_overs_left
        self._wickets_left = total_wickets_left
        self._number_of_balls_left_in_over = 0

    @property
    def batsman_on_strike(self):
        return self._batsman_on_strike

    @property
    def batsman_off_strike(self):
        return self._batsman_off_strike

    @property
    def runs_left_to_win(self):
        return self._runs_left_to_win

    @property
    def overs_left(self):
        return self._overs_left

    @property
    def wickets_left(self):
        return self._wickets_left

    @property
    def number_of_balls_left_in_over(self):
        return self._number_of_balls_left_in_over

    @property
    def match_table(self):
        return self._match_table

    @property
    def ball_log(self):
        return self._ball_log

    @property
    def player_queue(self):
        return self._player_queue

    def _validate_necessary_input_requirements(self):
        if self.total_runs_needed_to_win is None or self.total_runs_needed_to_win <= 0:
            raise ValidationError("Invalid value of runs_left_to_win found")
        if self.total_wickets_left is None or self.total_wickets_left <= 0:
            raise ValidationError("Invalid value of total_runs_needed_to_win found")
        if self.total_overs_left is None or self.total_overs_left <= 0:
            raise ValidationError("Invalid value of total_overs_left found")
        if self.total_wickets_left > self._player_queue.qsize() - 1:
            raise ValidationError("You need atleast {} players,"
                                  " right now you have added only {}".format(self.total_wickets_left + 1, self._player_queue.qsize()))
        return True

    def add_player(self, player):
        self._player_queue.put(player)
        self._match_table[player.id] = {
            'player': player,
            'balls_faced': 0,
            'runs_scored': 0,
            'has_not_played_yet': True
        }

    def _update_playing_status(self, player):
        self._match_table[player.id]['has_not_played_yet'] = False

    def _switch_strike(self):
        self._batsman_on_strike, self._batsman_off_strike = self._batsman_off_strike, self._batsman_on_strike

    def start_match_simulation(self):

        if self._validate_necessary_input_requirements():
            while not self.did_match_end():
                self.print_start_of_the_over_commentry()
                self._assign_batsman()
                outcome = self._get_ball_outcome()
                batsman_on_strike = self._batsman_on_strike
                self.process_outcome(outcome)
                self._update_ball_count_and_switch_strike()
                self._update_ball_log(batsman_on_strike, outcome)
                self.print_ball_commentry()

            self.print_match_table()
            self.print_end_of_the_match_status()

    def _update_ball_log(self, batsman_on_strike, outcome):
        self._ball_log.append(dict(
            over=(self.total_overs_left - self._overs_left - 1), # To convert from 4, 3.5, 3.4, ... 3.1 -> 0.1, 0.2, ...0.6
            ball=(6 - self._number_of_balls_left_in_over),
            player=batsman_on_strike,
            outcome=outcome
        ))

    # Reduce the ball count, if over complete
    # reduce the over and set the ball count
    def _update_ball_count_and_switch_strike(self):
        if self._number_of_balls_left_in_over != 0:
            self._number_of_balls_left_in_over -= 1
            if self._number_of_balls_left_in_over == 0:
                self._switch_strike()
        else:
            self._overs_left -= 1
            self._number_of_balls_left_in_over = 5

    def _get_ball_outcome(self):
        batting_outcome_probability = self._batsman_on_strike.batting_outcome_probability
        outcome = helper.weighted_choice(
            choices=batting_outcome_probability.keys(),
            weights=batting_outcome_probability.values()
        )
        return outcome

    def _assign_batsman(self):
        if self._batsman_on_strike is None:
            self._batsman_on_strike = self._player_queue.get()
            self._update_playing_status(self._batsman_on_strike)
        if self._batsman_off_strike is None:
            self._batsman_off_strike = self._player_queue.get()
            self._update_playing_status(self._batsman_off_strike)

    def is_any_balls_left(self):
        return not (
                self._overs_left == 0 and self._number_of_balls_left_in_over == 0)

    def is_any_wickets_left(self):
        return not (self._wickets_left <= 0)

    def is_any_runs_left_to_win(self):
        return not (self._runs_left_to_win <= 0)

    def did_match_end(self):
        return not self.is_any_balls_left() \
               or not self.is_any_wickets_left() \
               or not self.is_any_runs_left_to_win()

    def _update_player_balls_faced(self, player, value):
        self._match_table[player.id].update(
            balls_faced=self._match_table[player.id]['balls_faced'] + value)

    def _update_player_runs_scored(self, player, value):
        self._match_table[player.id].update(
            runs_scored=self._match_table[player.id]['runs_scored'] + value)

    def process_outcome(self, outcome):
        outcome_handler = {
            Outcomes.DOT: self.handle_dot,
            Outcomes.OUT: self.handle_out,
            Outcomes.SINGLE: self.handle_single,
            Outcomes.DOUBLE: self.handle_double,
            Outcomes.THREE: self.handle_three,
            Outcomes.FOUR: self.handle_four,
            Outcomes.FIVE: self.handle_five,
            Outcomes.SIX: self.handle_six
        }

        outcome_handler[outcome]()

    def handle_dot(self):
        self._update_player_balls_faced(player=self._batsman_on_strike, value=1)

    def handle_out(self):
        self._update_player_balls_faced(player=self._batsman_on_strike, value=1)
        self._batsman_on_strike = None
        self._wickets_left -= 1

    def handle_single(self):
        self._update_player_balls_faced(player=self._batsman_on_strike, value=1)
        self._update_player_runs_scored(player=self._batsman_on_strike, value=1)
        self._runs_left_to_win -= 1
        self._switch_strike()

    def handle_double(self):
        self._update_player_balls_faced(player=self._batsman_on_strike, value=1)
        self._update_player_runs_scored(player=self._batsman_on_strike, value=2)
        self._runs_left_to_win -= 2

    def handle_three(self):
        self._update_player_balls_faced(player=self._batsman_on_strike, value=1)
        self._update_player_runs_scored(player=self._batsman_on_strike, value=3)
        self._runs_left_to_win -= 3
        self._switch_strike()

    def handle_four(self):
        self._update_player_balls_faced(player=self._batsman_on_strike, value=1)
        self._update_player_runs_scored(player=self._batsman_on_strike, value=4)
        self._runs_left_to_win -= 4

    def handle_five(self):
        self._update_player_balls_faced(player=self._batsman_on_strike, value=1)
        self._update_player_runs_scored(player=self._batsman_on_strike, value=5)
        self._runs_left_to_win -= 5
        self._switch_strike()

    def handle_six(self):
        self._update_player_balls_faced(player=self._batsman_on_strike, value=1)
        self._update_player_runs_scored(player=self._batsman_on_strike, value=6)
        self._runs_left_to_win -= 6

    def print_ball_commentry(self):
        ball_log = self._ball_log[-1]
        print("{}.{} {} {}".format(ball_log['over'],
                                   ball_log['ball'],
                                   ball_log['player'].name,
                                   outcomes.outcome_to_commentry_text(
                                       outcome=ball_log['outcome'])
                                   ))

    def print_start_of_the_over_commentry(self):
        if self._number_of_balls_left_in_over == 0:
            print()
            print("{} overs left. {} runs to win. {} wickets left".format(
                self._overs_left, self._runs_left_to_win,
                self._wickets_left))

    def print_match_table(self):
        print()
        for _, player_stats in self._match_table.items():
            if player_stats['has_not_played_yet'] is False:
                print("{} - {}{} ({} balls)".format(player_stats['player'].name,
                                                    player_stats['runs_scored'],
                                                    "*" if player_stats[
                                                               'player'] in
                                                           [
                                                               self._batsman_on_strike,
                                                               self._batsman_off_strike]
                                                    else "",
                                                    player_stats[
                                                        'balls_faced']))
        print()

    def print_end_of_the_match_status(self):
        if self._runs_left_to_win <= 0:
            print("{} has won by {} wickets and {} balls remaining."
                  "They have made {}/{} needed runs".format(
                self.bating_team,
                self._wickets_left,
                self._overs_left * 6 + self._number_of_balls_left_in_over,
                self.total_runs_needed_to_win - self._runs_left_to_win,
                self.total_runs_needed_to_win
            ))
        elif self._runs_left_to_win == 1:
            print("{} - {} match is a draw!!".format(self.bating_team,
                                                   self.bowling_team))
        else:
            print("{} has lost with {} wickets in hand and {} balls to spare."
                  " They have made {}/{} needed runs".format(self.bating_team,
                                                             self._wickets_left,
                                                             self._overs_left * 6 + self._number_of_balls_left_in_over,
                                                             self.total_runs_needed_to_win - self._runs_left_to_win,
                                                             self.total_runs_needed_to_win))

