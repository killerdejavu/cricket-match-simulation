import unittest
from enum import Enum

import outcomes
from outcomes import Outcomes


class DefinedOutcomes(Enum):
    OUT = 'out'
    DOT = 'dot'
    SINGLE = 'single'
    DOUBLE = 'double'
    THREE = 'three'
    FOUR = 'four'
    FIVE = 'five'
    SIX = 'six'


class TestOutcomes(unittest.TestCase):

    def test_defined_outcomes(self):
        self.assertEqual([o.value for o in Outcomes], [o.value for o in DefinedOutcomes])

    def test_outcome_to_commentry_text(self):
        for o in Outcomes:
            self.assertNotEqual(outcomes.outcome_to_commentry_text(o), "")