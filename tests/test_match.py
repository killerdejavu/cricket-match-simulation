import copy
import unittest

from match import Match
from outcomes import Outcomes
from player import Player


class TestMatch(unittest.TestCase):

    def setUp(self):
        self.match = Match()
        self.player_dhoni = Player(name='M S Dhoni',
                                   batting_outcome_probability={
                                       Outcomes.DOT: 5,
                                       Outcomes.SINGLE: 30,
                                       Outcomes.DOUBLE: 25,
                                       Outcomes.THREE: 10,
                                       Outcomes.FOUR: 15,
                                       Outcomes.FIVE: 1,
                                       Outcomes.SIX: 9,
                                       Outcomes.OUT: 5
                                   })

        self.player_kohli = Player(name='Kohli', batting_outcome_probability={
            Outcomes.DOT: 5,
            Outcomes.SINGLE: 30,
            Outcomes.DOUBLE: 25,
            Outcomes.THREE: 10,
            Outcomes.FOUR: 15,
            Outcomes.FIVE: 1,
            Outcomes.SIX: 9,
            Outcomes.OUT: 5
        })

    def test_match_creation_defaults(self):
        self.assertEqual(self.match.bating_team, 'Bengaluru')
        self.assertEqual(self.match.bowling_team, 'Chennai')
        self.assertEqual(self.match.number_of_balls_left_in_over, 0)
        self.assertEqual(self.match.runs_left_to_win, 40)
        self.assertEqual(self.match.wickets_left, 3)
        self.assertEqual(self.match.overs_left, 4)

    def test_add_player(self):
        self.match.add_player(self.player_dhoni)
        self.assertEqual(self.match.player_queue.get(), self.player_dhoni)
        self.assertEqual(self.match.match_table[self.player_dhoni.id], {
            'player': self.player_dhoni,
            'balls_faced': 0,
            'runs_scored': 0,
            'has_not_played_yet': True
        })

    def test_handle_dot(self):
        self.match.add_player(self.player_dhoni)
        self.match.add_player(self.player_kohli)
        self.match._batsman_on_strike = self.player_dhoni
        self.match._batsman_off_strike = self.player_kohli

        match_table_before = copy.deepcopy(self.match.match_table)
        self.match.handle_dot()
        match_table_after = copy.deepcopy(self.match.match_table)

        self.assertEqual(
            match_table_before[self.player_dhoni.id]['balls_faced'] + 1,
            match_table_after[self.player_dhoni.id]['balls_faced'])
        self.assertEqual(
            match_table_before[self.player_dhoni.id]['runs_scored'],
            match_table_after[self.player_dhoni.id]['runs_scored'])

    def test_handle_out(self):
        self.match.add_player(self.player_dhoni)
        self.match.add_player(self.player_kohli)
        self.match._wickets_left = 3
        self.match._batsman_on_strike = self.player_dhoni
        self.match._batsman_off_strike = self.player_kohli

        match_table_before = copy.deepcopy(self.match.match_table)
        self.match.handle_out()
        match_table_after = copy.deepcopy(self.match.match_table)

        self.assertEqual(
            match_table_before[self.player_dhoni.id]['balls_faced'] + 1,
            match_table_after[self.player_dhoni.id]['balls_faced'])
        self.assertEqual(
            match_table_before[self.player_dhoni.id]['runs_scored'],
            match_table_after[self.player_dhoni.id]['runs_scored'])
        self.assertEqual(self.match._batsman_on_strike, None)
        self.assertEqual(self.match.wickets_left, 2)

    def test_handle_single(self):
        self.match.add_player(self.player_dhoni)
        self.match.add_player(self.player_kohli)
        self.match._runs_left_to_win = 10
        self.match._batsman_on_strike = self.player_dhoni
        self.match._batsman_off_strike = self.player_kohli

        match_table_before = copy.deepcopy(self.match.match_table)
        self.match.handle_single()
        match_table_after = copy.deepcopy(self.match.match_table)

        self.assertEqual(
            match_table_before[self.player_dhoni.id]['balls_faced'] + 1,
            match_table_after[self.player_dhoni.id]['balls_faced'])
        self.assertEqual(
            match_table_before[self.player_dhoni.id]['runs_scored'] + 1,
            match_table_after[self.player_dhoni.id]['runs_scored'])
        self.assertEqual(self.match._batsman_on_strike, self.player_kohli)
        self.assertEqual(self.match._batsman_off_strike, self.player_dhoni)
        self.assertEqual(self.match.runs_left_to_win, 9)

    def test_handle_double(self):
        self.match.add_player(self.player_dhoni)
        self.match.add_player(self.player_kohli)
        self.match._runs_left_to_win = 10
        self.match._batsman_on_strike = self.player_dhoni
        self.match._batsman_off_strike = self.player_kohli

        match_table_before = copy.deepcopy(self.match.match_table)
        self.match.handle_double()
        match_table_after = copy.deepcopy(self.match.match_table)

        self.assertEqual(
            match_table_before[self.player_dhoni.id]['balls_faced'] + 1,
            match_table_after[self.player_dhoni.id]['balls_faced'])
        self.assertEqual(
            match_table_before[self.player_dhoni.id]['runs_scored'] + 2,
            match_table_after[self.player_dhoni.id]['runs_scored'])
        self.assertEqual(self.match._batsman_on_strike, self.player_dhoni)
        self.assertEqual(self.match._batsman_off_strike, self.player_kohli)
        self.assertEqual(self.match.runs_left_to_win, 8)

