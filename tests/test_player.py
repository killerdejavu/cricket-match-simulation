from enum import Enum

from exception import ValidationError
from outcomes import Outcomes
from player import Player
import unittest


class TestPlayer(unittest.TestCase):

    def test_create_player(self):
        probabilities = {
            Outcomes.DOT: 5,
            Outcomes.SINGLE: 30,
            Outcomes.DOUBLE: 25,
            Outcomes.THREE: 10,
            Outcomes.FOUR: 15,
            Outcomes.FIVE: 1,
            Outcomes.SIX: 9,
            Outcomes.OUT: 5
        }
        name = "Some random name"
        player = Player(name=name, batting_outcome_probability=probabilities)

        self.assertEqual(player.name, name)
        self.assertEqual(player.batting_outcome_probability, probabilities)

    def test_create_player_fail_when_probabilities_dont_add_up(self):
        probabilities = {
            Outcomes.DOT: 5,
            Outcomes.SINGLE: 55,
            Outcomes.DOUBLE: 25,
            Outcomes.THREE: 10,
            Outcomes.FOUR: 15,
            Outcomes.FIVE: 1,
            Outcomes.SIX: 9,
            Outcomes.OUT: 5
        }
        name = "Some random name"

        with self.assertRaises(ValidationError) as cm:
            Player(name=name, batting_outcome_probability=probabilities)

        self.assertEqual(cm.exception.message, "Sum of all probabilities should be 100")

    def test_create_player_fail_when_not_supported_outcome(self):

        class RandOutcome(Enum):
            IN = 'in'

        probabilities = {
            Outcomes.DOT: 5,
            Outcomes.SINGLE: 55,
            Outcomes.DOUBLE: 25,
            Outcomes.THREE: 10,
            Outcomes.FOUR: 15,
            Outcomes.FIVE: 1,
            Outcomes.SIX: 9,
            RandOutcome.IN: 5
        }
        name = "Some random name"

        with self.assertRaises(ValidationError) as cm:
            Player(name=name, batting_outcome_probability=probabilities)

        self.assertEqual(cm.exception.message, "Unidentified outcome found - RandOutcome.IN")