import unittest

import helper


class TestHelper(unittest.TestCase):

    def test_weighted_choice(self):
        choices = [1, 2, 3]
        weights = [100, 0, 0]
        choice = helper.weighted_choice(choices=choices, weights=weights)
        self.assertEqual(choice, 1)

        choices = [1, 2, 3]
        weights = [50, 50, 0]
        choice = helper.weighted_choice(choices=choices, weights=weights)
        self.assertIn(choice, [1, 2])

        choices = [1, 2, 3]
        weights = [50, 50, 50]
        choice = helper.weighted_choice(choices=choices, weights=weights)
        self.assertIn(choice, [1, 2, 3])