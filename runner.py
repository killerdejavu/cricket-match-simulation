from match import Match
from outcomes import Outcomes
from player import Player


def main():
    match = Match(bating_team='Bengaluru', bowling_team='Chennai',
                  total_overs_left=4, total_wickets_left=3, total_runs_needed_to_win=40)

    outcome_probability = {
        Outcomes.DOT: 5,
        Outcomes.SINGLE: 30,
        Outcomes.DOUBLE: 25,
        Outcomes.THREE: 10,
        Outcomes.FOUR: 15,
        Outcomes.FIVE: 1,
        Outcomes.SIX: 9,
        Outcomes.OUT: 5
    }
    player = Player(name="Kirat Boli", batting_outcome_probability=outcome_probability)
    match.add_player(player)

    outcome_probability = {
        Outcomes.DOT: 10,
        Outcomes.SINGLE: 40,
        Outcomes.DOUBLE: 20,
        Outcomes.THREE: 5,
        Outcomes.FOUR: 10,
        Outcomes.FIVE: 1,
        Outcomes.SIX: 4,
        Outcomes.OUT: 10
    }
    player = Player(name="N.S Nodhi", batting_outcome_probability=outcome_probability)
    match.add_player(player)

    outcome_probability = {
        Outcomes.DOT: 20,
        Outcomes.SINGLE: 30,
        Outcomes.DOUBLE: 15,
        Outcomes.THREE: 5,
        Outcomes.FOUR: 5,
        Outcomes.FIVE: 1,
        Outcomes.SIX: 4,
        Outcomes.OUT: 20
    }
    player = Player(name="R Rumrah", batting_outcome_probability=outcome_probability)
    match.add_player(player)

    outcome_probability = {
        Outcomes.DOT: 30,
        Outcomes.SINGLE: 25,
        Outcomes.DOUBLE: 5,
        Outcomes.THREE: 0,
        Outcomes.FOUR: 5,
        Outcomes.FIVE: 1,
        Outcomes.SIX: 4,
        Outcomes.OUT: 30
    }
    player = Player(name="Shashi Henra", batting_outcome_probability=outcome_probability)
    match.add_player(player)

    # Assumption If you have 4 players, you can only have 3 wickets left,
    # the problem statement says 4 wickets
    match.start_match_simulation()


if __name__ == '__main__':
    main()
