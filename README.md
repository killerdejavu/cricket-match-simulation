## Cricket Match Simulation

### Dependencies

You need python3 to run this program.
If you use python2, you will face and error! 
There are no other dependencies

### How to run the program
python3 runner.py

### How to run the tests
python3 -m unittest

### Assumption
- In the problem, 4 wicket is specified and only 4 players are given. I am assuming this is 
a typo as you need (n + 1) players if you have n wickets left. Changed it to 3 
wickets
- 3 wickets left, 40 runs to win, 4 overs left are default parameters,
If you want to run with different params then
update runner.py line 7 (We can move to cli args as well)
- Have done only basic validation and tests, more tests could be added if time permits


### How should the output look like?
Bangalore Wins - ![Bangalore Wins](images/win.png)
Bangalore Loses - ![Bangalore Loses](images/loss.png)

