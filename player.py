import uuid
from enum import Enum

from exception import ValidationError
from outcomes import Outcomes


class Player(object):
    def __init__(self, name, batting_outcome_probability):
        self.name = name
        self._id = uuid.uuid4()
        self._batting_outcome_probability = None
        self.batting_outcome_probability = batting_outcome_probability

    @staticmethod
    def _validate_outcome_probability(value):
        if not value:
            raise ValidationError("Every players needs the batting_outcome_probability")

        if not isinstance(value, dict):
            raise ValidationError("Your batting_outcome_probability should be of type dict, "
                                  "e.g. {Outcomes.DOT: 30}")

        sum = 0
        for outcome, probability in value.items():

            # Assumed that probability is int and outcomes is Enum key,
            # if not we can validate that as well
            if not isinstance(outcome, Enum):
                raise ValidationError("each outcome should be of type Enum,"
                                      " usage should be like Outcomes.DOT")
            if outcome not in Outcomes:
                raise ValidationError(
                    "Unidentified outcome found - {}".format(outcome))
            if probability < 0 or probability > 100:
                raise ValidationError(
                    "Probability of a outcome should be between 0 and 100")
            sum += probability

        if sum != 100:
            raise ValidationError('Sum of all probabilities should be 100')

        return True

    @property
    def batting_outcome_probability(self):
        return self._batting_outcome_probability

    @batting_outcome_probability.setter
    def batting_outcome_probability(self, value):
        if self._validate_outcome_probability(value):
            self._batting_outcome_probability = value

    @property
    def id(self):
        return self._id

    def __repr__(self):
        return self.name
